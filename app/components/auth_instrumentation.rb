class AuthInstrumentation
  def instrument(type, field)
    if field.metadata[:requires_auth]
      old_resolve_proc = field.resolve_proc
      new_resolve_proc = lambda do |obj, args, ctx|
        raise  GraphQL::ExecutionError.new('Для выполнения этого действия требуется авторизация') unless ctx[:current_user]

        old_resolve_proc.call(obj, args, ctx)
      end
      field.redefine do
        resolve(new_resolve_proc)
      end
    else
      field
    end
  end
end