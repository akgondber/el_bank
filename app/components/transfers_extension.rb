module TransfersExtension
  def balance
    sum(:amount)
  end

  def balance_on_date(date)
    up_till(date).sum(:amount)
  end
end