class Auth
  ALGORITHM = 'HS256'

  class << self
    def issue(payload)
      JWT.encode(payload, auth_secret, ALGORITHM)
    end

    def decode(token)
      JWT.decode(token, auth_secret, true, algorithm: ALGORITHM).first.with_indifferent_access
    end

    def auth_secret
      Rails.application.credentials.secret_key_base
    end
  end
end