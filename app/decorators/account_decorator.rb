class AccountDecorator < SimpleDelegator
  def initialize(object, context = {})
    super(object)
    @on_date = context[:on_date] || Time.zone.now
  end

  def balance_on_date
    account_managements.balance_on_date(@on_date)
  end
end