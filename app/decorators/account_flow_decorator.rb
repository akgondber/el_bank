class AccountFlowDecorator < SimpleDelegator
  def initialize(object, params)
    super(object)
    @from = params[:from]
    @to = params[:to] || Time.zone.now
  end

  def account_managements
    __getobj__.account_managements.since(@from).until(@to)
  end

  def receipts
    received_transfers.since(@from).until(@to)
  end

  def transfers
    sent_transfers.since(@from).until(@to)
  end
end