class AccountManagement < ApplicationRecord
  belongs_to :account

  enum kind: {
    withdrawal: 'withdrawal',
    deposit: 'deposit'
  }

  scope :until, ->(date_time) { where('created_at <= ?', date_time) }
  scope :since, ->(date_time) { where('created_at >= ?', date_time) }
end
