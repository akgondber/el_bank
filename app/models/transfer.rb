class Transfer < ApplicationRecord
  belongs_to :recipient_account, class_name: 'Account', inverse_of: :received_transfers
  belongs_to :sender_account, class_name: 'Account', inverse_of: :sent_transfers

  validates :amount, presence: true, numericality: { greater_than: 0 }

  scope :until, ->(date_time) { where('created_at <= ?', date_time) }
  scope :since, ->(date_time) { where('created_at >= ?', date_time) }

  class << self
    alias_method :up_till, :until
  end
end
