class Account < ApplicationRecord
  belongs_to :user

  has_many :received_transfers, foreign_key: :recipient_account_id, class_name: 'Transfer', extend: TransfersExtension, dependent: :destroy
  has_many :sent_transfers, foreign_key: :sender_account_id, class_name: 'Transfer', extend: TransfersExtension, dependent: :destroy

  has_many :account_managements, dependent: :destroy do
    def balance
      deposit.sum(:amount) - withdrawal.sum(:amount)
    end

    def balance_on_date(date)
      deposit.until(date).sum(:amount) - withdrawal.until(date).sum(:amount)
    end
  end

  validates :code, presence: true

  after_initialize :generate_code, if: :new_record?

  def balance
    account_managements.balance + balance_in_transfers
  end

  def balance_on_date(date)
    account_managements.balance_on_date(date) + balance_in_transfers_on_date(date)
  end

  def balance_in_transfers
    received_transfers.balance - sent_transfers.balance
  end

  def balance_in_transfers_on_date(date)
    received_transfers.balance_on_date(date) - sent_transfers.balance_on_date(date)
  end

  private

  def generate_code
    loop do
      generated_code = SecureRandom.hex(5)

      unless self.class.exists?(code: generated_code)
        self.code = generated_code
        break
      end
    end
  end
end
