class Mutations::CreateAccount < GraphQL::Function
  type Types::AccountType

  def call(_obj, _args, ctx)
    ctx[:current_user].accounts.create!
  end
end