class Mutations::MakeWithdrawal < GraphQL::Function
  argument :amount, !types.Float
  argument :account_code, !types.String

  type Types::AccountManagementType

  def call(_obj, args, ctx)
    account = ctx[:current_user].accounts.find_by!(code: args[:account_code])
    result = PerformWithdrawal.call(account: account, amount: args[:amount])

    if result.success?
      result.account_management
    else
      GraphQL::ExecutionError.new(result.error)
    end
  end
end