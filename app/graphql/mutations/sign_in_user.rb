class Mutations::SignInUser < GraphQL::Function
  argument :username, !types.String
  argument :password, !types.String

  type Types::SignInType

  def call(_obj, args, _ctx)
    user = User.find_by(username: args[:username])

    if user.authenticate(args[:password])
      token = Auth.issue(user_id: user.id)
      OpenStruct.new(token: token)
    else
      GraphQL::ExecutionError.new('Неправильное сочетание username/password')
    end
  end
end