class Mutations::MakeDeposit < GraphQL::Function
  argument :amount, !types.Float, description: 'Сумма для зачисления в рублях'
  argument :account_code, !types.String

  type Types::AccountManagementType

  def call(_obj, args, ctx)
    account = ctx[:current_user].accounts.find_by!(code: args[:account_code])
    account.account_managements.deposit.create!(amount: args[:amount])
  end
end