class Mutations::MakeTransfer < GraphQL::Function
  argument :amount, !types.Float
  argument :recipient_account_code, !types.String
  argument :sender_account_code, !types.String

  type Types::TransferType

  def call(_obj, args, ctx)
    sender_account = ctx[:current_user].accounts.find_by!(code: args[:sender_account_code])
    recipient_account = Account.find_by!(code: args[:recipient_account_code])
    result = PerformTransfer.call(sender_account: sender_account, recipient_account: recipient_account, amount: args[:amount])

    if result.success?
      result.transfer
    else
      GraphQL::ExecutionError.new(result.error, options: { errors: result.errors })
    end
  end
end