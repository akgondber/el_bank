class Mutations::SignUpUser < GraphQL::Function
  argument :username, !types.String
  argument :password, !types.String
  argument :first_name, types.String
  argument :last_name, types.String

  type Types::UserType

  def call(_obj, args, _ctx)
    user = User.new(username: args[:username], password: args[:password])

    if user.save
      user
    else
      GraphQL::ExecutionError.new(user.errors.full_messages.join('; '))
    end
  end
end