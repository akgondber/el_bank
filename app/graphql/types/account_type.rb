Types::AccountType = GraphQL::ObjectType.define do
  name 'Account'

  field :id, !types.ID
  field :balance, !types.Float
  field :code, !types.String
  field :created_at, types.String
  field :user, -> { Types::UserType }
end