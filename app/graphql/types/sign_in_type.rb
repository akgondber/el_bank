Types::SignInType = GraphQL::ObjectType.define do
  name 'SignIn'

  field :token, !types.String
end