Types::AccountOnDateType = GraphQL::ObjectType.define do
  name 'AccountOnDate'

  field :id, !types.ID
  field :balance_on_date, !types.Float
  field :code, !types.String
  field :user, -> { Types::UserType }
end