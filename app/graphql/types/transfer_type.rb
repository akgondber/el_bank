Types::TransferType = GraphQL::ObjectType.define do
  name 'Transfer'

  field :id, !types.ID
  field :amount, !types.Float
  field :created_at, !types.String
end