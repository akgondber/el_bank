Types::MutationType = GraphQL::ObjectType.define do
  name 'Mutations'

  with_options requires_auth: true do
    field :create_account, function: Mutations::CreateAccount.new
    field :make_deposit, function: Mutations::MakeDeposit.new, description: 'Внести средства на счет'
    field :make_withdrawal, function: Mutations::MakeWithdrawal.new
    field :make_transfer, function: Mutations::MakeTransfer.new
  end

  field :sign_up_user, function: Mutations::SignUpUser.new
  field :sign_in_user, function: Mutations::SignInUser.new
end
