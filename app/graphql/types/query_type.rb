Types::QueryType = GraphQL::ObjectType.define do
  name 'Query'
  # Add root-level fields here.
  # They will be entry points for queries on your schema.

  with_options requires_auth: true do
    field :me, function: Resolvers::GetMe.new
    field :accounts, function: Resolvers::GetAccounts.new
    field :account_flow, function: Resolvers::GetAccountFlow.new
    field :account, !Types::AccountType do
      argument :id, !types.ID

      resolve -> (obj, args, ctx) {
        ctx[:current_user].accounts.find(args[:id])
      }
    end
    field :account_on_date, !Types::AccountOnDateType, description: 'Аккаунт с балансом на указанную дату' do
      argument :code, !types.String, description: 'код аккаунта'
      argument :on_date, !types.String

      resolve -> (obj, args, ctx) {
        AccountDecorator.new(ctx[:current_user].accounts.find_by!(code: args[:code]), on_date: args[:on_date])
      }
    end
  end
end
