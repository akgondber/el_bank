Types::AccountFlowType = GraphQL::ObjectType.define do
  name 'AccountFlow'

  field :id, !types.ID
  field :code, !types.String
  field :account_managements, !types[Types::AccountManagementType], 'Управление счетом (внесение и списание средств)'
  field :receipts, !types[Types::TransferType], description: 'Поступления на счет'
  field :transfers, !types[Types::TransferType], description: 'Переводы на другие счета'
end