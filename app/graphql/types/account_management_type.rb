Types::AccountManagementType = GraphQL::ObjectType.define do
  name 'AccountManagement'

  field :id, !types.ID
  field :kind, !types.String
  field :amount, !types.Float
  field :created_at, !types.String
  field :account, !Types::AccountType
end