class Resolvers::GetAccountFlow < GraphQL::Function
  argument :account_code, !types.String
  argument :from, !types.String
  argument :to, types.String

  type !Types::AccountFlowType

  def call(obj, args, ctx)
    account = ctx[:current_user].accounts.find_by!(code: args[:account_code])
    AccountFlowDecorator.new(account, from: args[:from], to: args[:to])
  end
end