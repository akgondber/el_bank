class Resolvers::GetAccounts < GraphQL::Function
  type !types[Types::AccountType]

  def call(obj, _args, ctx)
    ctx[:current_user].accounts
  end
end