GraphQL::Field.accepts_definitions requires_auth: GraphQL::Define.assign_metadata_key(:requires_auth)

class ElBankSchema < GraphQL::Schema
  mutation(Types::MutationType)
  query(Types::QueryType)

  # instrument(:query, AuthInstrumentation.new)
  instrument(:field, AuthInstrumentation.new)
end
