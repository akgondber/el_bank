class PerformWithdrawal
  include Interactor

  delegate :account, :amount, to: :context

  def call
    if account.balance >= amount
      context.account_management = account.account_managements.withdrawal.create!(amount: amount)
    else
      context.fail!(error: 'Сумма баланса меньше запрошенной суммы')
    end
  end
end