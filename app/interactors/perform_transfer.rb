class PerformTransfer
  include Interactor

  delegate :recipient_account, :sender_account, :amount, to: :context

  def call
    check_amount!
    ensure_different_accounts!
    check_balance_of_sender_account!
    make_transfer!
  end

  private

  def check_amount!
    return if amount.positive?

    context.fail!(error: 'Сумма перевода должна быть больше 0')
  end

  def ensure_different_accounts!
    return unless sender_account == recipient_account

    context.fail!(error: 'Переводить средства можно только на разные счета')
  end

  def check_balance_of_sender_account!
    return if sender_account.balance >= amount

    context.fail!(error: 'Сумма баланса должна быть больше или равна сумме перевода')
  end

  def make_transfer!
    new_transfer = sender_account.sent_transfers.new(amount: amount, recipient_account: recipient_account)

    if new_transfer.save
      context.transfer = new_transfer
    else
      context.fail!(error: new_transfer.errors.full_messages.join('; '), errors: new_transfer.errors)
    end
  end
end