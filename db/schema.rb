# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_08_06_173633) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "account_managements", force: :cascade do |t|
    t.bigint "account_id", null: false
    t.decimal "amount", precision: 19, scale: 2, null: false
    t.string "kind", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_account_managements_on_account_id"
  end

  create_table "accounts", force: :cascade do |t|
    t.string "code", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["code"], name: "index_accounts_on_code", unique: true
    t.index ["user_id"], name: "index_accounts_on_user_id"
  end

  create_table "transfers", force: :cascade do |t|
    t.bigint "recipient_account_id", null: false
    t.bigint "sender_account_id", null: false
    t.decimal "amount", precision: 19, scale: 2, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["recipient_account_id"], name: "index_transfers_on_recipient_account_id"
    t.index ["sender_account_id"], name: "index_transfers_on_sender_account_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "username", null: false
    t.string "password_digest", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  add_foreign_key "account_managements", "accounts"
  add_foreign_key "accounts", "users"
  add_foreign_key "transfers", "accounts", column: "recipient_account_id"
  add_foreign_key "transfers", "accounts", column: "sender_account_id"
end
