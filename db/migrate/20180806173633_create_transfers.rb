class CreateTransfers < ActiveRecord::Migration[5.2]
  def change
    create_table :transfers do |t|
      t.references :recipient_account, foreign_key: { to_table: :accounts }, null: false
      t.references :sender_account, foreign_key: { to_table: :accounts }, null: false
      t.decimal :amount, precision: 19, scale: 2, null: false

      t.timestamps
    end
  end
end
