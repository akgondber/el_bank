class CreateAccountManagements < ActiveRecord::Migration[5.2]
  def change
    create_table :account_managements do |t|
      t.references :account, foreign_key: true, null: false
      t.decimal :amount, precision: 19, scale: 2, null: false
      t.string :kind, null: false

      t.timestamps
    end
  end
end
