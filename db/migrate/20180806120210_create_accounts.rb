class CreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      t.string :code, null: false, index: { unique: true }
      t.references :user, foreign_key: true, null: false

      t.timestamps
    end
  end
end
